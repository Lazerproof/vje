(function(window, document, $, undefined, foundation){

	// 'use strict';

	window.app = {};
	app.instances = {}; // Container to hold plugins instances

	app.init = function() {
		app.cacheSelectors();
		app.initEqualizer();
		app.initDropdowns();
		app.initCartDropdownScrollbar();
		app.initCartReveals();
		app.initMainSlider();
		app.initHomepageTabs();
		app.initCheckoutCustomerTabs();
		app.initOffcanvasTabs();
		app.toggleOrderRow();
		app.initPriceFilter();
		app.initRegisterReveal();
		app.initContactsMap();
		app.initLoginReveal();
		app.initResetPassReveal();
		app.handleResetPassForm();
		app.handleRegisterForm();
		app.initContactsCarousel();
		app.initOffcanvas();
		app.initProductQuickView();
		app.initProductMediaCarousel();
		app.initFooterMenu();
		app.initMainOffcanvas();
		app.initPhoneOffcanvas();
		app.initDiscountCoundown();
		app.initFeaturedProductsCarousel();
		app.initProductTabs();
		app.initProdcutMediaVideo();
		app.initProductMediaReveal();
		app.initRelatedProductCarousel();
		app.initProductQuickViewTabs();
		app.handleCheckoutOptionsSelect();
		app.handleLoginForm();
		app.handleProductCartRemove();
		app.handleCardSelection();
		app.handlePackSelection();
		app.handleCardAddToOrder();
		app.handleProductIntroActions();
		app.handleBodyResize();
		app.handleCatalogBrandsFilter();
		app.handleProductIntroNotification();
		app.handleRemoveFromWaitlist();
		app.handleCallbackForm();
		app.handleOffcanvasSearch();
		app.handleResetCatalogFilters();
	}

	app.cacheSelectors = function() {
		// cache our selectors here, can use them throughout the file
		app.selectors = {};
		app.selectors.$body = $('body');
		app.selectors.$window = $(window);
		app.selectors.$homepageTabsContainer = $('#homepage-tabs');
		app.selectors.$checkoutCustomerTabsContainer = $('#checkout-customer-tabs');
		app.selectors.$offcanvasTabsContainer = $('#offcanvasTabs');

	}


	app.toggleOrderRow = function() {
		var $toggler = $('.js-order-row-toggle');
		$toggler.on('click', function() {
			$(this).parent('.order__title').toggleClass('order__title--uncollapsed')
			$(this).closest('.js-order').find('.js-order-row').toggleClass('order__row--uncollapsed');
		})
	}

	app.handleOffcanvasSearch = function() {
		var $input = $('#offcanvasSearchInput');
		var $results = $('#offcanvasSearchResults');
		var $resultsBox = $('#offcanvasSearchResultsBox');

		function makeRequest() {
			var x = $input.val();
			return x.split('');
		}

		function setMaxHeight() {
			var height = $(window).innerHeight() - $('.search-offcanvas__box').outerHeight();
			$resultsBox.css('max-height', height);
		}

		function initPerfectScrollbar() {
			setMaxHeight();
			$resultsBox.perfectScrollbar({
				suppressScrollX: true,
			});
		}

		if($input.length) {
			$input.on('input', Foundation.util.throttle(function() {
				var results = makeRequest();
				if(results.length) {
					$results.show();
				} else {
					$results.hide();
				}
			}, 1000));
			initPerfectScrollbar();
			app.selectors.$body.on('resizeme.zf.trigger', setMaxHeight);
			$('.js-search-offcanvas').on('closed.zf.offcanvas', function() {
				$results.hide();
				$input.val('');
			});
		}
	}

	app.handleProductIntroActions = function() {
		$('.js-add-to-wishlist').on('click', function() {
			$(this).addClass('is-added-to-wishlist')
		})
		$('.js-add-to-cart').on('click', function() {
			$(this).addClass('is-added-to-cart')
		})
	}

	app.handleRemoveFromWaitlist = function() {
		$('.js-product-intro-remove').on('click', function() {
			$(this).closest('.product-intro').remove();
		})
	}

	app.handleLoginForm = function() {
		var $form = $('#loginForm');

		if($form.length) {
			app.instances.loginFormAbide = new Foundation.Abide($form, {
				liveValidate: true
			});
		}
	}

	app.handleRegisterForm = function() {
		var $form = $('#registerForm');

		if($form.length) {
			app.instances.registerFormAbide = new Foundation.Abide($form, {
				liveValidate: true
			});
		}
	}

	app.handleResetPassForm = function() {
		var $form = $('#resetPassForm');

		if($form.length) {
			app.instances.resetPassFormAbide = new Foundation.Abide($form, {
				liveValidate: true
			});
		}
	}

	app.initOffcanvas = function() {
		function initSearchOffcanvas() {
			var $element = $('.js-search-offcanvas');

			if($element.length) {
				var elem = new Foundation.OffCanvas($element, {
					transition: 'overlap',
					autoFocus: false,
					transitionTime: 500,
					contentScroll: false
					// forceTo: 'top'
				});

				$(elem.$element).on('opened.zf.offcanvas', function() {
					setTimeout(function() {
						elem.$element.find('input').focus();
					}, 500);
				});
			}

		}

		initSearchOffcanvas();
	}


	app.initHomepageTabs = function() {
		if (app.selectors.$homepageTabsContainer.length) {
			app.homepageTabs = new Foundation.Tabs(app.selectors.$homepageTabsContainer, {
				linkClass: 'homepage-tabs__header-title',
				linkActiveClass: 'homepage-tabs__header-title--active',
				panelClass: 'homepage-tabs__panel',
				panelActiveClass: 'homepage-tabs__panel--active',
			});
		}
	};

	app.initOffcanvasTabs = function() {
		if(app.selectors.$offcanvasTabsContainer.length) {
			app.offcanvasTabs = new Foundation.Tabs(app.selectors.$offcanvasTabsContainer, {
				linkClass: 'offcanvas-tabs__tab-title',
				linkActiveClass: 'offcanvas-tabs__tab-title--active',
				panelClass: 'offcanvas-tabs__tab-panel',
				panelActiveClass: 'offcanvas-tabs__tab-panel--active',
			});

			$(app.offcanvasTabs.$element).on('change.zf.tabs', function() {
				if(app.instances.userDrilldownMenu) {
					app.instances.userDrilldownMenu._resize();
				}
				if(app.instances.contentDrilldownMenu) {
					app.instances.contentDrilldownMenu._resize();
				}
				if(app.instances.catalogDrilldownMenu) {
					app.instances.catalogDrilldownMenu._resize();
				}
			});
		}
	}

	app.initCheckoutCustomerTabs = function() {
		if (app.selectors.$checkoutCustomerTabsContainer.length) {
			app.checkoutCustomerTabs = new Foundation.Tabs(app.selectors.$checkoutCustomerTabsContainer, {
				linkClass: 'customer-data__tab-title',
				linkActiveClass: 'customer-data__tab-title--active',
				panelClass: 'customer-data__tab',
				panelActiveClass: 'customer-data__tab--active',
			});
		}
	};

	app.initMainOffcanvas = function() {
		var $element = $('#mainOffcanvas');

		if($element.length) {
			var elem = new Foundation.OffCanvas($element, {
				transition: 'overlap',
				transitionTime: 500
			});

			app.initCatalogDrilldownMenu();
			app.initUserDrilldownMenu();
			app.initContentDrilldownMenu();

			$(elem.$element).on('opened.zf.offcanvas', function() {
				if(app.instances.userDrilldownMenu) {
					app.instances.userDrilldownMenu._resize();
				}
				if(app.instances.contentDrilldownMenu) {
					app.instances.contentDrilldownMenu._resize();
				}
				if(app.instances.catalogDrilldownMenu) {
					app.instances.catalogDrilldownMenu._resize();
				}
			});
		}
	}

	app.initPhoneOffcanvas = function() {
		var $element = $('#phoneOffcanvas');

		if($element.length) {
			var elem = new Foundation.OffCanvas($element, {
				transition: 'overlap',
				transitionTime: 500
			});
		}
	}

	app.initContactsCarousel = function() {
		$('#contactsCarousel')
		.slick({
			slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 3000,
			slidesToScroll: 1,
			dots: false,
			arrows: true,
			prevArrow: '<button type="button" class="slick-prev slick-prev--contacts">Previous</button>',
			nextArrow: '<button type="button" class="slick-next slick-next--contacts">Next</button>',
		});
	}

	app.initContactsMap = function() {
		var $mapContainer = $('#contactsMap');
		if($mapContainer.length) {
			loadjs('//maps.googleapis.com/maps/api/js?key=AIzaSyA8TM62a4saURkQFWzKrZh0YR_HcrDp0SM', {
				success: function() {
					var uluru = {lat: 48.290628, lng: 25.935075};
					var map = new google.maps.Map(document.getElementById('contactsMap'), {
						zoom: 18,
						center: uluru
					});
					var marker = new google.maps.Marker({
						position: uluru,
						map: map
					});
				}
			});
		}
	}

	app.initUserDrilldownMenu = function() {
		var $element = $('#offcanvasUserMenu');
		if($element.length) {
			app.instances.userDrilldownMenu = new Foundation.Drilldown($element, {
				backButton: "<li class='js-drilldown-back'><a tabindex='0'>Назад</a></li>",
				autoHight: true
			});
		}
	}

	app.initCatalogDrilldownMenu = function() {
		var $element = $('#offcanvasCatalogMenu');
		if($element.length) {
			app.instances.catalogDrilldownMenu = new Foundation.Drilldown($element, {
				backButton: "<li class='js-drilldown-back'><a tabindex='0'>Назад</a></li>",
				autoHight: true
			});
		}
	}

	app.initContentDrilldownMenu = function() {
		var $element = $('#offcanvasContentMenu');
		if($element.length) {
			app.instances.contentDrilldownMenu = new Foundation.Drilldown($element, {
				backButton: "<li class='js-drilldown-back'><a tabindex='0'>Назад</a></li>",
				autoHight: true
			});
		}

	}

	app.initFoundation = function() {
		$(document).foundation();
	}

	app.handleBodyResize = function() {
		app.selectors.$body.on('resizeme.zf.trigger', handleResize);

		function handleResize() {
			app.equlizeHieghtOfCardReveal();
			app.equlizeHieghtOfPackReveal();
		}

	}

	app.initPriceFilter = function() {
		var slider = document.getElementById('slider');

		if(slider) {
			app.priceFilter = noUiSlider.create(slider, {
				start: [50, 1500],
				connect: true,
				tooltips: true,
				range: {
					'min': 0,
					'max': 2500
				},
				format: wNumb({
					decimals: 0,
					postfix: ' грн.'
				})
			});
		}
	}

	app.initEqualizer = function() {
		$('.js-discounts-list__row').each(function() {
			new Foundation.Equalizer($(this), {
				equalizeByRow: true,
				equalizeOn: 'xmedium'
			});
		})

		$('.js-blog-list__row').each(function() {
			new Foundation.Equalizer($(this), {
				equalizeByRow: true,
				equalizeOn: 'medium'
			});
		})

		$('.js-order__row').each(function() {
			new Foundation.Equalizer($(this), {
				equalizeByRow: true,
				equalizeOn: 'xlarge'
			});
		})

		app.instances.checkoutEqualizer = new Foundation.Equalizer($('#checkout-options'), {
			equalizeByRow: true,
			equalizeOn: 'medium'
		});
	}

	app.handleProductIntroNotification = function() {
		var $link = $('.js-product-intro__notification-link');

		$link.on('click', handleClick);

		function handleClick(e) {
			e.preventDefault();
			var $target = $(e.currentTarget);
			var $notificationBox = $target.parent('.js-product-intro__notification');
			var $message = $notificationBox.next('.js-product-intro__notification-message');
			$target.parent($notificationBox).remove();
			$message.show();

		}
	}

	app.handleCheckoutOptionsSelect = function() {
		var $optionTypes = $('[data-checkout-option]');

		var showOptionData = function(option) {
			var itemChecked = $(option).data('checkout-option');
			$('[data-checkout-option-data]')
				.addClass('is-hidden');
			$('[data-checkout-option-data=' + itemChecked + ']')
				.removeClass('is-hidden');
		};

		if ($optionTypes.is(':checked')) {
			var $checked = $optionTypes.filter(':checked');
			showOptionData($checked);
		}

		$optionTypes.on('change', function() {
			showOptionData(this);
		});
	};

	app.handleProductCartRemove = function() {
		var $trigger = $('.js-cart-remove-product');

		$trigger.on('click', function(e) {
			removeProduct(e.currentTarget);
		})

		function removeProduct(item) {
			$(item).closest('.js-cart-product').remove();
		}

	}

	app.handleCardSelection = function() {
		$('[data-cart-card-item]')
			.on('click', function(event) {
				removeSelected();
				$(event.currentTarget)
					.addClass('cart-card-reveal__item--selected');
				showPreview(event.currentTarget);
			});

		function removeSelected() {
			$('[data-cart-card-item]')
				.removeClass('cart-card-reveal__item--selected');
		}

		function showPreview(item) {
			var $item = $(item);
			var itemPrice = $item.find('[data-cart-card-price]')
				.text();
			var itemImage = $item.find('[data-cart-card-image]')
				.attr('src');
			var itemSku = $item.data('cart-card-item');
			var itemTitle = $item.find('[data-cart-card-title]')
				.text();

			var $previewContainer = $('#cardPreviewContainer');
			$previewContainer.find('[data-cart-card-preview-price]')
				.text(itemPrice);
			$previewContainer.find('[data-cart-card-preview-title]')
				.text(itemTitle);
			$previewContainer.find('[data-cart-card-preview-sku]')
				.text(itemSku);
			$previewContainer.find('[data-cart-card-preview-image]')
				.attr('src', itemImage);
		}
	}

	app.handlePackSelection = function() {
		$('[data-cart-pack-item]')
			.on('click', function(event) {
				removeSelected();
				$(event.currentTarget)
					.addClass('cart-card-reveal__item--selected');
				showPreview(event.currentTarget);
			});

		function removeSelected() {
			$('[data-cart-pack-item]')
				.removeClass('cart-card-reveal__item--selected');
		}

		function showPreview(item) {
			var $item = $(item);
			var itemPrice = $item.find('[data-cart-pack-price]')
				.text();
			var itemImage = $item.find('[data-cart-pack-image]')
				.attr('src');
			var itemSku = $item.data('cart-pack-item');
			var itemTitle = $item.find('[data-cart-pack-title]')
				.text();

			var $previewContainer = $('#packPreviewContainer');
			$previewContainer.find('[data-cart-pack-preview-price]')
				.text(itemPrice);
			$previewContainer.find('[data-cart-pack-preview-title]')
				.text(itemTitle);
			$previewContainer.find('[data-cart-pack-preview-sku]')
				.text(itemSku);
			$previewContainer.find('[data-cart-pack-preview-image]')
				.attr('src', itemImage);
		}
	}


	app.handleCallbackForm = function() {
		var $callbackButton = $('#header-contacts__callback-button');
		var $callbackForm = $('#header-contacts__callback-form');
		var $callbackThank = $('#header-contacts__callback-thank');
		var $callbackLink = $('#header-contacts__callback-thank-link');
		var $callbackPopup = $('#header-contacts__callback-popup');

		$callbackButton.on('click', function(e) {
			e.preventDefault();
			$callbackForm.hide();
			$callbackThank.show();
		});

		$callbackLink.on('click', function(e) {
			e.stopImmediatePropagation();
			e.preventDefault();
			$callbackPopup.foundation('close');
			$callbackForm.show();
			$callbackThank.hide();
		});
	}


	app.handleCardAddToOrder = function() {
		var $items = $('.js-cart-card-reveal__add-link');

		$items.on('click', function(e) {
			e.stopImmediatePropagation();
			e.preventDefault();
			var target = e.currentTarget;
			toggleLinkClass(target);
			addToOrder(target);
		});

		function toggleLinkClass(link) {
			var $link = $(link);
			$link.toggleClass('cart-card-reveal__add-link--added');
		}

		function addToOrder(item) {
			console.log('Card added to order');
		}
	}

	app.handleResetCatalogFilters = function() {
		var $button = $('#resetCatalogFilters');
		$button.on('click', function() {
			$('#catalogBrandsFilter').multipleSelect('uncheckAll');
			app.priceFilter.reset();
		})
	}

	app.handleCatalogBrandsFilter = function() {
		var $catalogBrandsFilter = $('#catalogBrandsFilter');

		if($catalogBrandsFilter.length) {
			$catalogBrandsFilter
				.change(function() {
					console.log($(this)
						.val());
				})
				.multipleSelect({
					width: '100%',
					placeholder: 'Выбрать',
					allSelected: 'Выбраны все',
					countSelected: '# из %',
					selectAll: false,
					selectAllText: 'Выбрать все'
				});
		}

	}

	app.initCartCardListScrollbar = function(action) {
		if ($('[data-cart-card-item]').length) {
			var $cartCardList = $('#cartCardRevealList');
			if(action.type === 'open') {
				if (Foundation.MediaQuery.atLeast('xmedium')) {
					$cartCardList.perfectScrollbar({
						suppressScrollX: true,
					});
				}
			} else if (action.type === 'close') {
				$cartCardList.perfectScrollbar('destroy');
			}
		}
	}

	app.initCartPackListScrollbar = function(action) {
		if ($('[data-cart-pack-item]').length) {
			var $cartPackList = $('#cartPackRevealList');
			if(action.type === 'open') {
				if (Foundation.MediaQuery.atLeast('xmedium')) {
					$cartPackList.perfectScrollbar({
						suppressScrollX: true,
					});
				}
			} else if (action.type === 'close') {
				$cartPackList.perfectScrollbar('destroy');
			}
		}
	}

	app.equlizeHieghtOfCardReveal = function() {
		var $previewContainer = $('#cardPreviewContainer');
		var $cartCardList = $('#cartCardRevealList');
		var height = $previewContainer.outerHeight();
		if (Foundation.MediaQuery.atLeast('xmedium')) {
			$cartCardList.css('max-height', height);
		} else {
			$cartCardList.css('max-height', '');
		}
	}

	app.equlizeHieghtOfPackReveal = function() {
		var $previewContainer = $('#packPreviewContainer');
		var $cartPackList = $('#cartPackRevealList');
		var height = $previewContainer.outerHeight();
		if (Foundation.MediaQuery.atLeast('xmedium')) {
			$cartPackList.css('max-height', height);
		} else {
			$cartPackList.css('max-height', '');
		}
	}

	app.initFooterMenu = function() {
		var $columnHeader = $('.js-footer-column-header');

		function toggleClasses(e) {
			$(e.target).next().toggleClass('footer-menu--collapsed');
			$(e.target).toggleClass('footer__column-header--collapsed');
		}

		function applyClasses() {
			if (Foundation.MediaQuery.atLeast('xmedium')) {
				$columnHeader.next().removeClass('footer-menu--collapsed');
				$columnHeader.removeClass('footer__column-header--collapsed');
				$columnHeader.off('click', toggleClasses);
			} else {
				$columnHeader.off('click', toggleClasses);
				$columnHeader.next().addClass('footer-menu--collapsed');
				$columnHeader.addClass('footer__column-header--collapsed');
				$columnHeader.on('click', toggleClasses);
			}
		}

		applyClasses();

		$('body').on('resizeme.zf.trigger', applyClasses);
	}

	app.initRelatedProductCarousel = function() {
		if($('.js-product-related-slider').length) {
			$('.js-product-related-slider')
				.slick({
					slidesToShow: 1,
					mobileFirst: true,
					prevArrow: '<button type="button" class="slick-prev slick-prev--round">Previous</button>',
					nextArrow: '<button type="button" class="slick-next slick-next--round">Next</button>',
					responsive: [
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 4,
								slidesToScroll: 4,
								dots: false
							}
						}, {
							breakpoint: 620,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 3,
								dots: false
							}
						}, {
							breakpoint: 480,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 2,
								dots: false
							}
						}, {
							breakpoint: 320,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								dots: false
							}
						}
					]
				});
		}
	}

	app.initDiscountCoundown = function() {
		if($('.js-discount__countdown').length) {
			$('.js-discount__countdown').each(function (index) {
				var userLang = $(this).data('countdown-lang');
				var lang = (userLang ? userLang : 'ru');
				var userFinalTime = $(this).data('countdown-time');
				var finalTime = (userFinalTime ? userFinalTime : $(this).text());
				$.countdown.setDefaults($.countdown.regionalOptions[lang]);
				$(this).countdown({
					until: new Date(finalTime),
					layout: "{d<}<div class='discount__countdown-item days'>"+
					   "<span class='discount__countdown-item-value'>{dn}</span>" +
					   "<span class='discount__countdown-item-label'>{dl}</span>" +
					   "</div>{d>}" +
					   "<div class='discount__countdown-item hours'>" +
					   "<span class='discount__countdown-item-value'>{hn}</span>" +
					   "<span class='discount__countdown-item-label'>{hl}</span>" +
					   "</div>" +
					   "<div class='discount__countdown-item minutes'>" +
					   "<span class='discount__countdown-item-value'>{mn}</span>" +
					   "<span class='discount__countdown-item-label'>{ml}</span>" +
					   "</div>" +
					   "<div class='discount__countdown-item seconds'>" +
					   "<span class='discount__countdown-item-value'>{sn}</span>" +
					   "<span class='discount__countdown-item-label'>{sl}</span>" +
					   "</div>",
				});
			})
		}
	}

	app.initProductTabs = function() {
		var $productTabsContainer = $('#product-tabs');

		if ($productTabsContainer.length) {
			var productTabs = new Foundation.Tabs($productTabsContainer, {
				responsiveAccordionTabs: 'accordion large-tabs',
				linkClass: 'product-tabs__title',
				linkActiveClass: 'product-tabs__title--active',
				panelClass: 'product-tabs__panel',
				panelActiveClass: 'product-tabs__panel--active',
			});
		}
	}

	app.initProductQuickViewTabs = function() {
		var $productTabsContainer = $('#product-quick-view-tabs');

		if ($productTabsContainer.length) {
			var productTabs = new Foundation.Tabs($productTabsContainer, {
				responsiveAccordionTabs: 'accordion large-tabs',
				linkClass: 'product-tabs__title',
				linkActiveClass: 'product-tabs__title--active',
				panelClass: 'product-tabs__panel',
				panelActiveClass: 'product-tabs__panel--active',
			});
		}
	}

	app.initLoginReveal = function() {
		var $loginReveal = $('#loginReveal');

		if ($loginReveal.length) {
			app.instances.loginReveal = new Foundation.Reveal($loginReveal, {
				vOffset: 0
			});

		}
	}

	app.initRegisterReveal = function() {
		var $registerReveal = $('#registerReveal');

		if ($registerReveal.length) {
			app.instances.registerReveal = new Foundation.Reveal($registerReveal, {
				vOffset: 0
			});

		}
	}

	app.initResetPassReveal = function() {
		var $resetPass = $('#resetPassReveal');

		if ($resetPass) {
			app.instances.resetPassReveal = new Foundation.Reveal($resetPass, {
				vOffset: 0
			});

		}
	}

	app.initCartReveals = function() {
		var $cartCardReveal = $('#cartCardReveal');
		var $cartPackReveal = $('#cartPackReveal');
		app.instances.cartCardReveal = new Foundation.Reveal($cartCardReveal, {
			vOffset: 0
		});
		app.instances.cartPackReveal = new Foundation.Reveal($cartPackReveal, {
			vOffset: 0
		});
		// Card reveal
		$cartCardReveal.on('open.zf.reveal', app.initCartCardListScrollbar);
		$cartCardReveal.on('open.zf.reveal', app.equlizeHieghtOfCardReveal);
		$cartCardReveal.on('closed.zf.reveal', app.initCartCardListScrollbar);
		// Pack reveal
		$cartPackReveal.on('open.zf.reveal', app.initCartPackListScrollbar);
		$cartPackReveal.on('open.zf.reveal', app.equlizeHieghtOfPackReveal);
		$cartPackReveal.on('closed.zf.reveal', app.initCartPackListScrollbar);
	}

	app.initCartDropdownScrollbar = function() {
		if ($('[data-header-cart-product]')
			.length) {
			$('[data-header-cart-product-container]')
				.perfectScrollbar();
		}
	}

	app.initProductMediaReveal = function() {
		var $productMediaReveal = $('#product-media-reveal');
		var productMediaReveal = new Foundation.Reveal($productMediaReveal);
		var $productImages = $('.js-product-images-in-reveal');
		var $productThumbs = $('.js-product-thumbnails-in-reveal');
		var imageOptions = {
			slidesToShow: 1,
			slidesToScroll: 1,
			prevArrow: '<button type="button" class="slick-prev slick-prev--product-media">Previous</button>',
			nextArrow: '<button type="button" class="slick-next slick-next--product-media">Next</button>',
			arrows: true,
			fade: true,
			asNavFor: '.js-product-thumbnails-in-reveal'
		}

		var thumbOptions = {
			asNavFor: '.js-product-images-in-reveal',
			slidesToShow: 4,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			mobileFirst: true,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 320,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 1,
					}
				}, {
					breakpoint: 468,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 1,
						vertical: true
					}
				}, {
					breakpoint: 650,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 1,
						vertical: true
					}
				}, {
					breakpoint: 767,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 1,
						vertical: true
					}
				}, {
					breakpoint: 960,
					settings: {
						slidesToShow: 7,
						slidesToScroll: 1,
						vertical: true
					}
				}, {
					breakpoint: 1023,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 1,
						vertical: true
					}
				}
			]
		};

		$(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
			if(newSize === 'xxmedium') {
				$productImages.slick('slickSetOption', imageOptions, true);
				$productThumbs.slick('slickSetOption', thumbOptions, true);
				if($productMediaReveal.length) {
					productMediaReveal.$element.foundation('close');
				}
			}

		});


		$($productMediaReveal).on('open.zf.reveal', function() {
			$productImages.slick('slickSetOption', imageOptions, true);
			$productThumbs.slick('slickSetOption', thumbOptions, true);

		});

		$($productMediaReveal).on('close.zf.reveal', function() {
			var players = plyr.get('.js-product-videos-in-reveal')
			players.forEach(function(player) {
  				player.pause();
  			})

		});



		$productImages.slick(imageOptions);
		$productThumbs.slick(thumbOptions);
	}

	app.initDropdowns = function() {
		var $categoriesDropdownContainers = $('.js-categories-nav__dropdown');
		var $callbackDropdownContainer = $('#header-contacts__callback-popup');
		var $cartDropdownContainer = $('#header-cart-dropdown');
		var $userDropdownContainer = $('#user-account-dropdown');
		var $productDeliveryDropdownContainer = $('#productConditionsDelivey');
		var $productPaymentDropdownContainer = $('#productConditionsPayment');
		var $productWarrantyDropdownContainer = $('#productConditionsWarranty');
		var $productDeliveryDropdownContainerQv = $('#productConditionsDeliveryQv');
		var $productPaymentDropdownContainerQv = $('#productConditionsPaymentQv');
		var $productWarrantyDropdownContainerQv = $('#productConditionsWarrantyQv');

		if ($categoriesDropdownContainers.length) {
			$categoriesDropdownContainers.each(function() {
				new Foundation.Dropdown($(this), {
					hover: true,
					hoverPane: true,
					positionClass: 'bottom',
				});
			})

		}

		if ($callbackDropdownContainer.length) {
			var $callbackDropdown = new Foundation.Dropdown($callbackDropdownContainer, {
				hover: true,
				hoverPane: true,
				vOffset: 12,
				positionClass: 'right bottom',
			});
		}

		if ($userDropdownContainer.length) {
			var $cartDropdown = new Foundation.Dropdown($userDropdownContainer, {
				hover: true,
				hoverPane: true,
				vOffset: 15,
				positionClass: 'right bottom',
			});
		}

		if ($cartDropdownContainer.length) {
			var $cartDropdown = new Foundation.Dropdown($cartDropdownContainer, {
				hover: true,
				hoverPane: true,
				vOffset: 25,
			});
		}

		if ($productDeliveryDropdownContainer.length) {
			var $productDeliveryDropdown = new Foundation.Dropdown($productDeliveryDropdownContainer, {
				hover: true,
				hoverPane: true,
				vOffset: 20,
				positionClass: 'bottom',
				parentClass: 'product-conditions'
			});
		}

		if ($productDeliveryDropdownContainerQv.length) {
			var $productDeliveryDropdownQv = new Foundation.Dropdown($productDeliveryDropdownContainerQv, {
				hover: true,
				hoverPane: true,
				vOffset: 20,
				positionClass: 'bottom',
				parentClass: 'product-conditions'
			});
		}

		if ($productPaymentDropdownContainer.length) {
			var $productPaymentDropdown = new Foundation.Dropdown($productPaymentDropdownContainer, {
				hover: true,
				hoverPane: true,
				vOffset: 20,
				positionClass: 'bottom',
				parentClass: 'product-conditions'
			});
		}

		if ($productPaymentDropdownContainerQv.length) {
			var $productPaymentDropdownQv = new Foundation.Dropdown($productPaymentDropdownContainerQv, {
				hover: true,
				hoverPane: true,
				vOffset: 20,
				positionClass: 'bottom',
				parentClass: 'product-conditions'
			});
		}

		if ($productWarrantyDropdownContainer.length) {
			var $productWarrantyDropdownQv = new Foundation.Dropdown($productWarrantyDropdownContainer, {
				hover: true,
				hoverPane: true,
				vOffset: 20,
				positionClass: 'bottom',
				parentClass: 'product-conditions'
			});
		}

		if ($productWarrantyDropdownContainerQv.length) {
			var $productWarrantyDropdown = new Foundation.Dropdown($productWarrantyDropdownContainerQv, {
				hover: true,
				hoverPane: true,
				vOffset: 20,
				positionClass: 'bottom',
				parentClass: 'product-conditions'
			});
		}
	}

	app.initProdcutMediaVideo = function() {
		var players = plyr.setup('.js-product-videos, .js-product-videos-in-reveal, .js-quick-view-videos');
		$('.js-product-images, .js-product-images-in-reveal, .js-product-quick-view-images').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		  players.forEach(function(player) {
			  player.pause();
		  })
		});
	}

	app.initProductMediaCarousel = function() {
		var $productImages = $('.js-product-images');
		var $productThumbnails = $('.js-product-thumbnails');
		var imageOptions = {
			slidesToShow: 1,
			slidesToScroll: 1,
			adaptiveHeight: true,
			arrows: false,
			fade: true,
			asNavFor: '.js-product-thumbnails'
		}

		var thumbOptions = {
			asNavFor: '.js-product-images',
			slidesToShow: 4,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			mobileFirst: true,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 320,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 1,
					}
				}, {
					breakpoint: 468,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 1,
						vertical: true
					}
				}, {
					breakpoint: 650,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 1,
						vertical: true
					}
				}, {
					breakpoint: 767,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 1,
						vertical: false
					}
				}, {
					breakpoint: 960,
					settings: {
						slidesToShow: 7,
						slidesToScroll: 1,
						vertical: false
					}
				}, {
					breakpoint: 1023,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 1,
						vertical: true,
					}
				}
			]
		}

		$productImages.slick(imageOptions);
		$productThumbnails.slick(thumbOptions);

		$(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
			$productImages.slick('slickSetOption', imageOptions, true);
			$productThumbnails.slick('slickSetOption', thumbOptions, true);
		});
	}

	app.initMainSlider = function() {
		var $slider = $('#mainSlider');


		function setSlideHeight() {
			var $slide = $('.js-main-slider__slide-inner');
			if (!Foundation.MediaQuery.atLeast('medium')) {
				var height = $(window).innerHeight();
				var headerHeight = $('.header').innerHeight();
				var newHeight = height - headerHeight;
				$slide.height(newHeight);
			} else {
				$slide.height('');
			}
		}


		if($slider.length) {
			$slider.on('init', function(){
				setSlideHeight();
			});

			$slider.slick({
				slidesToShow: 1,
				autoplay: true,
				autoplaySpeed: 3000,
				slidesToScroll: 1,
				dots: true,
				arrows: false,
				dotsClass: 'slick-dots slick-dots--main-slider'
			});

			app.selectors.$body.on('resizeme.zf.trigger', setSlideHeight);
		}
	}

	app.initFeaturedProductsCarousel = function() {
		$('#featured-products__box')
			.slick({
				slidesToShow: 1,
				autoplay: true,
				autoplaySpeed: 2000,
				slidesToScroll: 1,
				dots: false,
				arrows: false,
				mobileFirst: true,
				focusOnSelect: true,
				responsive: [
					{
						breakpoint: 520,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1,
						}
					}, {
						breakpoint: 1024,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
						}
					}
				]
			});
	}

	app.initProductQuickView = function() {
		var $productQuickViewReveal = $('#product-quick-view');
		var productQuickViewReveal = new Foundation.Reveal($productQuickViewReveal);
		var $productImages = $('.js-product-quick-view-images');
		var $productThumbs = $('.js-product-quick-view-thumbnails');
		var imageOptions = {
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.js-product-quick-view-thumbnails'
		}

		var thumbOptions = {
			asNavFor: '.js-product-quick-view-images',
			slidesToShow: 4,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			mobileFirst: true,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 1,
					}
				}, {
					breakpoint: 1200,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 1,
					}
				}
			]
		};

		$(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
			if(newSize === 'xxmedium') {
				$productImages.slick('slickSetOption', imageOptions, true);
				$productThumbs.slick('slickSetOption', thumbOptions, true);
				if($productQuickViewReveal.length) {
					productQuickViewReveal.$element.foundation('close');
				}
			}

		});


		$($productQuickViewReveal).on('open.zf.reveal', function() {
			$productImages.slick('slickSetOption', imageOptions, true);
			$productThumbs.slick('slickSetOption', thumbOptions, true);

		});

		$($productQuickViewReveal).on('close.zf.reveal', function() {
			var players = plyr.get('.js-quick-view-videos')
			players.forEach(function(player) {
  				player.pause();
  			})

		});



		$productImages.slick(imageOptions);
		$productThumbs.slick(thumbOptions);
	}
})(window, document, jQuery, Foundation);
